$(document).ready(function(){

  var bannerSlider = $('#bannerSlider');
	var isModuleBannerActive = bannerSlider !== null;

	if (isModuleBannerActive) {
	  console.log('Banner Carousel Module active');

    bannerSlider.owlCarousel({
          items:1,
          loop:true,
          controls:true,
          lazyLoad: true,
          autoplay: true,
          autoplayTimeout: 6000,
          animateOut: 'fadeOut',
          itemElement: "div"

    });

	} else {
	  console.log('The Banner Carousel Module does not exists in the page.');
	}

});
