$(document).ready(function(){

	'use strict';
	// console.log("videoPlayer js loaded");
	var videoWrap = document.querySelector('.video-wrap');

	var isModuleVideoActive = videoWrap !== null;

	if (isModuleVideoActive) {
	  console.log('Video module active');

		var	bodyEl 		= document.body,
				videoEl 	= document.querySelector('video'),
				playCtrl 	= document.querySelector('.action--play'),
				closeCtrl = document.querySelector('.action--close');

		function init() {
			initEvents();
		}

		function initEvents() {
			playCtrl.addEventListener('click', play);
			closeCtrl.addEventListener('click', hide);
			videoEl.addEventListener('loadedmetadata', allowPlay);
			videoEl.addEventListener('ended', hide);
		}
		if(videoEl.readyState > 1){
			allowPlay();
		}
		function allowPlay() {
			console.log("allowPlay notmal");
			classie.add(bodyEl, 'video-loaded');
		}

		function play() {
			videoEl.currentTime = 0;
			classie.remove(videoWrap, 'video-wrap--hide');
			classie.add(videoWrap, 'video-wrap--show');
			setTimeout(function() {videoEl.play();}, 600);
		}

		function hide() {
			classie.remove(videoWrap, 'video-wrap--show');
			classie.add(videoWrap, 'video-wrap--hide');
			videoEl.pause();
		}

		init();

	} else {
		console.log('The video module does not exists in the page.');
	}

});
