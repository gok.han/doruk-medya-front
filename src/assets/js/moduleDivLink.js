$(document).ready(function() {
  // JavaScript querySelector for single element
  var select = function(s) {
    return document.querySelector(s);
  };

  // JavaScript querySelectorAll
  var selectAll = function(s) {
    return document.querySelectorAll(s);
  };

  //News page rows
  $(".newsItemRow").click(function() {
    divLink($(this));
  });

  // Homepage news sectioni news items
  $(".homeNewsItem").click(function() {
    divLink($(this));
  });

  // converts whole div to link items with the ahref tag
  function divLink(target) {
    window.location = target.find("a").attr("href");
    return false;
  }

  //
  // var zoomItems = select( '.hoverZoom' );
  // console.log("zoomItems ", zoomItems);

  // for (var l = 0; l < zoomItems.length; l++) {
  //   // TweenMax.set(zoomItems[l],{backgroundSize:"100% auto"})
  //   // TweenMax.to(zoomItems[l], .4, {backgroundSize: "+=20% +=20%",autoRound:false, ease:Expo.easeOut});
  //
  // }
  //
  // var $box = $('.homeNewsItem');
  // console.log($box);
  // console.log($box.children());
  // console.log($box.find(".newsThumbMedium"));
  // // console.log($box.children());
  // TweenMax.set($box.find(".newsThumbMedium"), {backgroundPosition:"center"});
  // $box.hover(
  //    function() {
  //
  //       TweenMax.to($(this).find(".newsThumbMedium"), 2, {backgroundPosition:"left", ease:Circ.easeOut});
  //       // TweenMax.to($(this).find(".newsThumbMedium"), 0.3, {scale:1.2});
  //       // TweenMax.to($(this).children(1), 0.3, {scale:1.2});
  //    },
  //    function() {
  //       TweenMax.to($(this).find(".newsThumbMedium"), 0.15, {backgroundPosition:"center"});
  //       // TweenMax.to($(this).find(".newsThumbMedium"), 0.15, {scale:1});
  //       // TweenMax.to($(this).children(1), 0.15, {scale:1});
  //    }
  // );
  // zoomItems.hover(
  //    function() {
  //       TweenMax.to($(this), 0.3, {scale:1.2});
  //    },
  //    function() {
  //       TweenMax.to($(this), 0.15, {scale:1});
  //    }
  // );
  // function init() {
  // 	initEvents();
  // 	setInitialState();
  // }
  //
  //
  // init();
  // openMenu();
  // tweenME();
});
