$(document).ready(function(){
	console.log('offCanvasMenu Loaded');

	var dummy = $("#dummy");

	// JavaScript querySelector for single element
	var select = function(s) {
	    return document.querySelector(s);
	};

	// JavaScript querySelectorAll
	var selectAll = function(s) {
	    return document.querySelectorAll(s);
	};

	var bodyEl = document.body,
			content = select ( '#site-canvas' ),
			openbtn = select( '#open-button' ),
			closebtn = select( '#close-button' ),
			isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.addEventListener( 'click', toggleMenu );
		if( closebtn ) {
			closebtn.addEventListener( 'click', toggleMenu );
		}

		// close the menu element if the target it´s not the menu element or one of its descendants..
		content.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( isOpen && target !== openbtn ) {
			toggleMenu();
			}
		} );
	}

	function toggleMenu() {
		if( isOpen ) {
			classie.toggle( bodyEl, 'show-menu' );
			classie.toggle(openbtn, 'is-active');
			//console.log('Menu Closed, Button Hamburger');
		}
		else {
			classie.toggle( bodyEl, 'show-menu' );
			classie.toggle(openbtn, 'is-active');
			//console.log('Menu Opened, Button Cross');
		}
		isOpen = !isOpen;
	}

	init();
	// openMenu();
	// tweenME();
});
