$(document).ready(function(){

  var logoCarousel = $('#logoCarousel');
	var isModuleLogoActive = logoCarousel !== null;

	if (isModuleLogoActive) {
	  console.log('Logo Carousel Module active');

    logoCarousel.owlCarousel({
      loop:true,
      margin:20,
      nav: false,
      autoplay: true,
      autoplayTimeout: 2500,
      responsiveClass:true,
      nav:false,
      responsive:{
        0:{
            items:2,
            autoplayTimeout: 1500,
        },
        640:{
            items:3,
        },
        1024:{
            items:5,
        }
      }
    });

	} else {
	  console.log('The Logo Carousel Module does not exists in the page.');
	}

});
